#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: ecdsa 1.2.0 ruby lib

Gem::Specification.new do |s|
  s.name = "ecdsa".freeze
  s.version = "1.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 2".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["David Grayson".freeze]
  s.date = "2014-09-14"
  s.email = "davidegrayson@gmail.com".freeze
  s.files = ["Gemfile".freeze, "LICENSE.txt".freeze, "README.md".freeze, "lib/ecdsa.rb".freeze, "lib/ecdsa/format.rb".freeze, "lib/ecdsa/format/decode_error.rb".freeze, "lib/ecdsa/format/field_element_octet_string.rb".freeze, "lib/ecdsa/format/integer_octet_string.rb".freeze, "lib/ecdsa/format/point_octet_string.rb".freeze, "lib/ecdsa/format/signature_der_string.rb".freeze, "lib/ecdsa/group.rb".freeze, "lib/ecdsa/group/nistp192.rb".freeze, "lib/ecdsa/group/nistp224.rb".freeze, "lib/ecdsa/group/nistp256.rb".freeze, "lib/ecdsa/group/nistp384.rb".freeze, "lib/ecdsa/group/nistp521.rb".freeze, "lib/ecdsa/group/secp112r1.rb".freeze, "lib/ecdsa/group/secp112r2.rb".freeze, "lib/ecdsa/group/secp128r1.rb".freeze, "lib/ecdsa/group/secp128r2.rb".freeze, "lib/ecdsa/group/secp160k1.rb".freeze, "lib/ecdsa/group/secp160r1.rb".freeze, "lib/ecdsa/group/secp160r2.rb".freeze, "lib/ecdsa/group/secp192k1.rb".freeze, "lib/ecdsa/group/secp192r1.rb".freeze, "lib/ecdsa/group/secp224k1.rb".freeze, "lib/ecdsa/group/secp224r1.rb".freeze, "lib/ecdsa/group/secp256k1.rb".freeze, "lib/ecdsa/group/secp256r1.rb".freeze, "lib/ecdsa/group/secp384r1.rb".freeze, "lib/ecdsa/group/secp521r1.rb".freeze, "lib/ecdsa/point.rb".freeze, "lib/ecdsa/prime_field.rb".freeze, "lib/ecdsa/recover_public_key.rb".freeze, "lib/ecdsa/sign.rb".freeze, "lib/ecdsa/signature.rb".freeze, "lib/ecdsa/verify.rb".freeze, "lib/ecdsa/version.rb".freeze]
  s.homepage = "https://github.com/DavidEGrayson/ruby_ecdsa".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "This gem implements the Ellipctic Curve Digital Signature Algorithm (ECDSA) almost entirely in pure Ruby.".freeze
end
